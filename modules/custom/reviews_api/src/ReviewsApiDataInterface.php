<?php

namespace Drupal\reviews_api;

/**
 * Interface for xxxxxxxx data plugins.
 */
interface ReviewsApiDataInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label(): string;

  /**
   * Extract the rows relevant to this plugin.
   *
   * @param array $record
   *   The record from which to extract the rows.
   *
   * @return array
   *   The rows relevant to this plugin.
   */
  public function extractRows(array $record): array;

}
