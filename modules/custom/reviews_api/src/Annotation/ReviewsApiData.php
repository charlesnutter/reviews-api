<?php

namespace Drupal\reviews_api\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a ReviewsApiData annotation object.
 *
 * @Annotation
 */
class ReviewsApiData extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The API needed for the plugin.
   *
   * @var string|array
   */
  public $api;

  /**
   * The ID of the field to use as a key in merge database requests.
   *
   * @var string
   */
  public $key;

}
