<?php

namespace Drupal\reviews_api\Exception;

/**
 * Exception when a request fails because of an invalid or missing token.
 */
class NewTokenNeededException extends \Exception {}
