<?php

namespace Drupal\reviews_api;

/**
 * Defines an interface for xxxxxxxxx request classes.
 */
interface ReviewsApiRequestInterface {

  const REVIEWSAPI_API_DOMAIN = 'api1.consumerism.xxxxxxxx.com';

  const COMMENTS_DATA_PATH = '/api/bsr/comments';
  const REVIEWS_DATA_PATH = '/api/external/bsr/v2/reviews';
  const TOKEN_DATA_PATH = '/api/service/v1/token/create';

  /**
   * Fetch a result from a request to xxxxxxxx.
   *
   * @param array $options
   *   Options for the request, which may include:
   *     - type: the type of reviews ("physician" or "location").
   *     - token: The access token to use.
   *     - query: The query parameters to use in the request.
   *     - count: The amount of providers requested ("single" or "multiple").
   *
   * @return array
   *   The result of the request.
   */
  public function fetchFromRequest(array $options = []);

}
