<?php

namespace Drupal\reviews_api\Commands;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\reviews_api\ReviewsApiDataUpdater;
use Drush\Commands\DrushCommands;

/**
 * Defines Drush commands for xxxxxxxx commands.
 */
class ReviewsApiCommands extends DrushCommands {

  use StringTranslationTrait;

  /**
   * The xxxxxxxx data updater.
   *
   * @var \Drupal\reviews_api\ReviewsApiDataUpdater
   */
  protected ReviewsApiDataUpdater $reviewsApiDataUpdater;

  /**
   * reviewsApiCommands constructor.
   *
   * @param \Drupal\reviews_api\ReviewsApiDataUpdater $reviews_api_data_updater
   *   The xxxxxxxx data updater.
   */
  public function __construct(ReviewsApiDataUpdater $reviews_api_data_updater) {
    $this->reviewsApiDataUpdater = $reviews_api_data_updater;
  }

  /**
   * Update the Import Source database with data from xxxxxxxx.
   *
   * @param string $api
   *   The API to use. (e.g., "comments" or "reviews").
   * @param array $options
   *   Additional options for the command.
   *
   * @option plugins A comma-delimited list of plugins to run.
   *   If omitted, all plugins will be run.
   *
   * @command reviews:api:update
   *
   * @usage reviews:api:update comments
   *   Update provider review data.
   */
  public function updateData(
    $api,
    array $options = [
      'plugins' => self::REQ,
    ]
  ) {
    $start_message = $this->t('Starting fetch of review data from xxxxxxxx');
    $this->logger()->notice($start_message);

    $plugins = $options['plugins'] ? explode(',', $options['plugins']) : [];

    try {
      $this->reviewsApiDataUpdater->update($api, $plugins);
    }
    catch (\Exception $exception) {
      $error_message = $this->t('Failed to fetch xxxxxxxx data due to error "%error"', ['%error' => $exception->getMessage()]);

      $this->logger()->log('error', $error_message);
      return;
    }

    $end_message = $this->t('Successfully fetched data from xxxxxxxx');
    $this->logger()->success($end_message);
  }

}
