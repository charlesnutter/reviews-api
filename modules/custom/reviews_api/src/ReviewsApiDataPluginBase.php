<?php

namespace Drupal\reviews_api;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for xxxxxxxx data plugins.
 */
abstract class ReviewsApiDataPluginBase extends PluginBase implements ReviewsApiDataInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getKey(): string {
    return $this->pluginDefinition['key'];
  }

  /**
   * {@inheritdoc}
   */
  public function extractRows(array $record): array {
    // This function should be overridden.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function execute(array $data) {
    $key = $this->getKey();

    $rows = [];
    foreach ($data as $record) {
      $rows = array_merge($rows, $this->extractRows($record));
    }

    foreach ($rows as $row) {
      if (!empty($row)) {
        // Save the row to the database.
        $this->saveToDatabase($key, $row);
      }
    }
  }

  /**
   * Save the fields to the Import Source database.
   *
   * @param string $key
   *   The ID of the field to use as a key.
   * @param array $fields
   *   The fields to save, in key-value pairs.
   */
  protected function saveToDatabase(string $key, array $fields) {
    // Switch to the import_source database and get the database connection.
    Database::setActiveConnection('import_source');
    $connection = Database::getConnection();

    // Update the record identified by the key. By convention, the table name
    // is the same as the plugin ID and must be created accordingly in the
    // import source schema.
    $table = $this->getId();
    $connection->merge($table)
      ->key($key, $fields[$key])
      ->fields($fields)
      ->execute();

    // Switch back to the default database.
    Database::setActiveConnection('');
  }

}
