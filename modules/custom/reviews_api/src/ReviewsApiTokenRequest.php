<?php

namespace Drupal\reviews_api;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\Exception\RequestException;

/**
 * Handles performing an HTTP request to fetch a token from xxxxxxxx.
 */
class ReviewsApiTokenRequest extends ReviewsApiRequestBase {

  const REQUEST_MAX_NUM = 5;

  /**
   * {@inheritdoc}
   */
  public function fetchFromRequest(array $options = []) {

    $url = $this->calculateEndpoint('token');

    // Get the xxxxxxxx customer credentials.
    $credentials = $this->configFactory->get('reviews_api.credentials')->get('live');

    // Define the request options.
    // @todo need to store this data within the secret file on Acquia.
    $options = [
      'form_params' => [
        'appId' => $credentials['client_id'],
        'appSecret' => $credentials['client_secret'],
      ],
    ];

    // Ensure we our under our maximum connection limit.
    while (!$this->dataFetched && $this->requestCount < self::REQUEST_MAX_NUM) {
      $this->requestCount++;

      try {
        $body = (string) $this->httpClient
          ->post($url, $options)
          ->getBody();
      }
      catch (RequestException $exception) {
        if ($this->requestCount >= self::REQUEST_MAX_NUM) {
          $args = ['%url' => $url, '%error' => $exception->getMessage()];
          throw new \Exception(new FormattableMarkup('Error fetching token from URL %url due to error "%error', $args));
        }
      }

      // Ensure we have a token, otherwise circle back around.
      $body = Json::decode($body);
      $token = $body['accessToken'] ?: '';

      if (!empty($token)) {
        $this->dataFetched = TRUE;
      }
    }

    // Reset the dataFetched property.
    $this->dataFetched = FALSE;

    return $token ?: '';
  }

}
