<?php

namespace Drupal\reviews_api;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\reviews_api\Exception\NewTokenNeededException;
use GuzzleHttp\Exception\RequestException;

/**
 * Handles performing an HTTP request to fetch data from xxxxxxxx.
 */
class ReviewsApiDataRequest extends ReviewsApiRequestBase {

  const REQUEST_MAX_NUM = 5;

  /**
   * The count of the number of request attempts.
   *
   * @var int
   */
  protected $ourRequests = 0;

  /**
   * {@inheritdoc}
   */
  public function fetchFromRequest(array $options = []): array {

    $api = $options['api'] ?? 'comments';
    $token = $options['token'];
    $query = $options['query'];
    $provider_ids = $options['query']['provider_ids'] ?: FALSE;

    if (!$provider_ids) {
      return FALSE;
    }
    else {
      $provider_ids = implode(',', $options['query']['provider_ids']);
    }

    $url = $this->calculateEndpoint($api);

    $query = [
      'form_params' => [
        'personId' => $provider_ids,
        'perPage' => '500',
      ],
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded',
        'Access-Token' => $token,
      ],
    ];

    $data = [];
    while (!$this->dataFetched && $this->requestCount < self::REQUEST_MAX_NUM) {
      $this->requestCount++;

      try {
        $body = (string) $this->httpClient
          ->post($url, $query)
          ->getBody();
      }
      catch (RequestException $exception) {

        // Try again on request exception unless the request limit was reached.
        if ($this->requestCount >= self::REQUEST_MAX_NUM) {
          $args = ['%url' => $url, '%error' => $exception->getMessage()];
          throw new \Exception(new FormattableMarkup('Error fetching data from URL %url due to error "%error"', $args));
        }
        continue;
      }

      try {
        $data = $this->serializer->decode($body, 'json');
        // xxxxxxxx places their status codes within their response, so we
        // need to throw exceptions based on the returned value.
        if (isset($data['status']['code'])) {
          switch ($data['status']['code']) {
            case "401":
              print 'Invalid response from API or bad token' . PHP_EOL;
              throw new \Exception();
          }
        }
      }
      // @todo This exception needs context.
      catch (NewTokenNeededException) {
        throw new NewTokenNeededException();
      }

      if (isset($data)) {
        $this->dataFetched = TRUE;
      }
    }

    // Reset the dataFetched property.
    $this->dataFetched = FALSE;

    // Reset the request count.
    $this->requestCount = 0;

    return $data;
  }

}
