<?php

namespace Drupal\reviews_api\Plugin\ReviewsApiData;

use Drupal\Component\Serialization\Json;
use Drupal\reviews_api\ReviewsApiDataPluginBase;

/**
 * Plugin implementation of reviews data from xxxxxxxx.
 *
 * @ReviewsApiData(
 *   id = "provider_reviews",
 *   label = @Translation("Provider Reviews"),
 *   key = "reviews_api_id",
 * )
 */
class ProviderReviews extends ReviewsApiDataPluginBase {

  /**
   * {@inheritdoc}
   *
   * Until all providers move to xxxxxxxx, providers with less than 30
   * reviews or ratings are weeded out via migrate conditions and theme
   * checks on the respective the fields. Here they pass through with little
   * logic.
   */
  public function extractRows(array $record): array {
    $reviews_api_id = $record['id'] ?? '';
    $reviews = !empty($record['comments']) ? Json::encode($record['comments']) : '';
    $comment_count = $record['totalCommentCount'];
    $rating_count = $record['totalRatingCount'];
    $overall_rating = $record['overallRating']['value'] ?? '';

    $rows = [];
    $rows[$reviews_api_id] = [
      'reviews' => $reviews,
      'reviews_api_id' => $reviews_api_id,
      'comment_count' => $comment_count,
      'rating_count' => $rating_count,
      'overall_rating' => $overall_rating,
      'status' => 'active',
    ];

    return $rows;
  }

}
