/**
 * @file
 * Javascript to reveal hidden reviews incrementally.
 *
 * @see src/components/provider-reviews/review-card.twig
 *
 */

((Drupal, once) => {
  Drupal.behaviors.providerReviews = {
    attach(context) {
      // Check that the load more button exists.
      const moreReviews = once('showReviews', '.provider-reviews');

      if (moreReviews.length) {
        const hiddenReviews = [
          ...document.querySelectorAll('.provider-review.hidden'),
        ];
        const showReviews = document.getElementById('showReviews');

        showReviews.addEventListener('click', (e) => {
          e.preventDefault();

          hiddenReviews
            .splice(0, 5)
            .forEach((elem) => elem.classList.remove('hidden'));

          // Hide the load more button if there are no more hidden reviews.
          if (hiddenReviews.length == 0) {
            showReviews.classList.add('hidden');
          }
        });
      }
    },
  };
})(Drupal, once);
