# Provider Reviews API Code Sample #

This work was for a recent client. I needed to consume an API that held review data for a couple thousand medical providers and display the data the client's website. The client managed all of their provider data through one vendor, but was in the process of transferring to a new vendor— this began the process by incorporating provider reviews.

The code contains two custom-built modules, files from the theme relating to the reviews, a YAML file that was used in the migration process, and a bash script.

This codes's been anonymized.

## Modules ##
### Reviews API Module ###
The bulk of the API functionality is within this module. Some notes:

#### Approach ####
The module utilizes two interfaces, one to request data and one to transform it and store it in an intermediary database. Because of resource limitations set by the vendor, the calls to the api are broken into chunks. The retrieved data is then transformed by a plugin manager that implements according to the context of the data involved. At this point in the process, the plugin manager is using only a single plugin— the plan was to introduce new plugins as new types of data (locations, provider details beyond reviews) were imported.

### Twig Extension Module ###
The theme requires a count of the imported reviews, which are stored in a JSON array. This twig extension parses the array and provides a count.

### Migrate Plus YAML file ###
Once the data is stored in a sort of staging database; it's migrated into the site via the migrate plus module on a weekly basis.

### Bash Script ###
There is a bash script that was run to execute a drush command to grab the data. It was handled via Acquia.

## Theme ##
Any code that isn't relevant to the Reviews API has been removed from the theme, so it appears a bit barren; the structure was a component-based theme that transformed data at the template level and passed it on to components. The backbone of the styles is Tailwind. There is an attached javascript library as well.
