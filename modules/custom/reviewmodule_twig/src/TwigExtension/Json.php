<?php

namespace Drupal\reviewmodule_twig\TwigExtension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Provides a twig extension to decode json strings.
 *
 * The extension is a single-purpose implementation of the json_decode php
 * function. It's used decode API data (reviews) into arrays to be parsed by the
 * theme.
 *
 * @todo migrate away from native php support to Drupal's serialization class.
 */
class Json extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFilters(): array {
    return [
      new TwigFilter('json_decode', [$this, 'jsonDecode']),
      new TwigFilter('json_count_items', [$this, 'jsonCountItems']),
    ];
  }

  /**
   * Decodes the json string. Returns an empty array if any error is thrown.
   *
   * @param string $json
   *   The json string supplied in the twig file.
   */
  public function jsonDecode(string $json): array {
    $json = json_decode($json, TRUE);
    if (json_last_error() === JSON_ERROR_NONE) {
      return $json;
    }
    return [];
  }

  /**
   * Counts the top-level elements in a decoded Json string.
   *
   * @param string $json
   *   The json string supplied in the twig file.
   */
  public function jsonCountItems(string $json): int {
    $json = $this->jsonDecode($json);
    $json = count($json, 0);

    return $json;
  }

}
