<?php

namespace Drupal\reviews_api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * A base class for performing HTTP requests to a xxxxxxxx endpoint.
 */
abstract class ReviewsApiRequestBase implements ReviewsApiRequestInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected Serializer $serializer;

  /**
   * The count of the number of request attempts.
   *
   * @var int
   */
  protected int $requestCount = 0;

  /**
   * Whether the data has been successfully fetched.
   *
   * @var bool
   */
  protected bool $dataFetched = FALSE;

  /**
   * Constructs a ReviewsApiRequest object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Symfony\Component\Serializer\Serializer $serializer
   *   The serializer.
   */
  public function __construct(ClientInterface $http_client, StateInterface $state, ConfigFactoryInterface $config_factory, Serializer $serializer) {
    $this->httpClient = $http_client;
    $this->state = $state;
    $this->configFactory = $config_factory;
    $this->serializer = $serializer;
  }

  /**
   * Determine which endpoint should be used for a request.
   *
   * @param string $api
   *   The api requested ("comments", "reviews", and "token").
   *
   * @return string
   *   The calculated endpoint.
   */
  protected function calculateEndpoint(string $api = 'comments'): string {
    $endpoint = 'https://';

    $endpoint .= self::REVIEWSAPI_API_DOMAIN;

    switch ($api) {
      case 'comments':
        $endpoint .= self::COMMENTS_DATA_PATH;
        break;

      case 'reviews':
        $endpoint .= self::REVIEWS_DATA_PATH;
        break;

      case 'token':
        $endpoint .= self::TOKEN_DATA_PATH;
        break;
    }

    return $endpoint;
  }

}
