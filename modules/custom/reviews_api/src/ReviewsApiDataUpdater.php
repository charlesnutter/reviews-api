<?php

namespace Drupal\reviews_api;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\reviews_api\Exception\NewTokenNeededException;

/**
 * Updates the Import Source database with data from xxxxxxxx.
 */
class ReviewsApiDataUpdater {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The "xxxxxxxx data" plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected PluginManagerInterface $pluginManager;

  /**
   * The xxxxxxxx data request service.
   *
   * @var \Drupal\reviews_api\ReviewsApiRequestInterface
   */
  protected ReviewsApiRequestInterface $dataRequest;

  /**
   * The xxxxxxxx token request service.
   *
   * @var \Drupal\reviews_api\ReviewsApiRequestInterface
   */
  protected ReviewsApiRequestInterface $tokenRequest;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The array of provider ids.
   *
   * @var array
   */
  protected array $providerIds = [];

  /**
   * The ReviewsApiDataUpdater constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $plugin_manager
   *   The plugin manager.
   * @param \Drupal\reviews_api\ReviewsApiRequestInterface $data_request
   *   The xxxxxxxx data request service.
   * @param \Drupal\reviews_api\ReviewsApiRequestInterface $token_request
   *   The xxxxxxxx token request service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
      PluginManagerInterface $plugin_manager,
      RequestApiRequestInterface $data_request,
      RequestApiRequestInterface $token_request,
      StateInterface $state,
      ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->pluginManager = $plugin_manager;
    $this->dataRequest = $data_request;
    $this->tokenRequest = $token_request;
    $this->state = $state;
  }

  /**
   * Update the Import Source database with data from xxxxxxxx.
   *
   * @param string $api
   *   The requested api.
   * @param array $plugins
   *   A list of plugins to run.
   */
  public function update(string $api, array $plugins = []) {

    // Get data for providers with reviews enabled. These values come from
    // Kyruus.
    $reviews_enabled = $this->entityTypeManager
      ->getStorage('node')
      ->loadByProperties([
        'type' => 'provider',
        'status' => 1,
        'field_pro_display_reviews' => 1,
      ]);

    $provider_npi_data = [];

    // Get the NPI values from the provider data. xxxxxxxx uses the NPI for
    // the provider IDs.
    foreach ($reviews_enabled as $provider) {
      $provider_npi_data[] = $provider->field_pro_npi->value;
    }

    // xxxxxxxx will tend to time out with calls with over 15 providers.
    $this->providerIds = array_chunk($provider_npi_data, 15);

    while ($data = $this->fetchReviews()) {
      // Allow each plugin to extract what it needs from each record.
      foreach (array_keys($this->pluginManager->getDefinitions()) as $plugin_id) {
        $plugin = $this->pluginManager->createInstance($plugin_id);

        // Retrieve the database key and comments.
        $key = $plugin->getKey();
        $data = $data['data']['entities'];

        $rows = [];
        foreach ($data as $record) {
          $rows = array_merge($rows, $plugin->extractRows($record));
        }

        $this->saveToDatabase($plugin_id, $key, $rows);
      }
      array_shift($this->providerIds);
    }
  }

  /**
   * Fetch data from xxxxxxxx.
   *
   * @return array
   *   The fetched data.
   */
  protected function fetchReviews(): array {
    $data = [];

    // If we've reached the last set in the array, return with no data.
    if (count($this->providerIds) == 0) {
      return $data;
    }

    // Pass the latest token, along with the subset of provider IDs.
    $options = [
      'token' => $this->getLatestToken(),
      'query' => [
        'provider_ids' => $this->providerIds[0],
      ],
    ];

    if (is_null($options['token'])) {
      $this->setLatestToken();
      $options['token'] = $this->getLatestToken();
    }

    try {
      $data = $this->dataRequest->fetchFromRequest($options);
    }
    // If the latest token is invalid, fetch a new token, save it, and try
    // fetching the data again.
    catch (NewTokenNeededException $exception) {
      $this->setLatestToken();
      $options['token'] = $this->getLatestToken();
      $data = $this->dataRequest->fetchFromRequest($options);
    }
    return $data;
  }

  /**
   * Save the fields to the Import Source database.
   *
   * @param string $table
   *   The table to which the data should be saved.
   * @param string $key
   *   The ID of the field to use as a key.
   * @param array $data
   *   The data to save, in key-value pairs.
   */
  protected function saveToDatabase($table, $key, array $data) {
    // Switch to the import_source database and get the database connection.
    Database::setActiveConnection('import_source');
    $connection = Database::getConnection();

    // Get field names dynamically.
    $first_element = reset($data);
    $field_names = array_keys($first_element);

    $upsert = $connection->upsert($table)
      ->fields($field_names)
      ->key($key);

    foreach ($data as $row) {
      $upsert->values($row);
    }

    $upsert->execute();

    // Switch back to the default database.
    Database::setActiveConnection('');
  }

  /**
   * Get the latest xxxxxxxx access token.
   *
   * @return string
   *   The latest token.
   */
  protected function getLatestToken() {
    return $this->state->get('reviews_api.token');
  }

  /**
   * Set the xxxxxxxx access token.
   */
  protected function setLatestToken() {

    $token = $this->tokenRequest->fetchFromRequest();

    $this->state->set('reviews_api.token', $token);
    $this->state->set('reviews_api.token_updated', date('c'));
  }

}
